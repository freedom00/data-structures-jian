/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipd.jacksontest;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author ipd
 */
public class jacksontest {
    
    public static void main(String[] args) throws IOException {
        File jsonFile= new File("user.json");
        
        ObjectMapper mapper= new ObjectMapper();
        
        User user=null;
        user =mapper.readValue(jsonFile, User.class);
        
        System.err.println(user.getName().getFirst());
        
        
        String jsonString="{\"name\": {\"first\":\"Michael\",\"last\":\"Lishaa\" },\"gender\":\"male\", \"verified\":false" +"}";
        user =mapper.readValue(jsonString, User.class);
        
        System.err.println(user.getName().getLast());
        
        
    }
    
}
