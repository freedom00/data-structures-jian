/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day02wordsfreq;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author ipd
 */
public class Day02WordsFreq {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //System.out.println("DSDFSDFS");

        try {
            Scanner fileInput = new Scanner(new File("pg10.txt")); //use delimiters [ \t\r\n\\....]
            //fileInput.useDelimiter("[ \\t\\r\\n\\.,:;-?]");
            fileInput.useDelimiter("\\Z");
            String content = "";
            if (fileInput.hasNext()) {
                content = fileInput.next();
            }
            HashMapSort(content);
            System.out.println("=========================================================================");
            TreeMapSort(content);
        } catch (FileNotFoundException ex) {
            System.out.println("" + ex.getMessage());
        }
    }

    static void HashMapSort(String content) {
        long startTime = System.currentTimeMillis();
        HashMap<String, Integer> myMap = new HashMap<String, Integer>();
        //content ="wo d sd 2ew 'sf 91. ZH";
        int start = 0;
        String word = "";
        boolean isword = false;
        int endOfLine = content.length() - 1;

        for (int i = 0; i < content.length(); i++) {
            // if the char is a letter, word = true.
            if (Character.isLetter(content.charAt(i)) && i != endOfLine) {
                isword = true;
                // if char isn't a letter and there have been letters before,
                // counter goes up.
            } else if (!Character.isLetter(content.charAt(i))) {
                if (isword) {
                    word = content.substring(start, i);
                    //word = word.replace(" ", "");
                    myMap.putIfAbsent(word, 0);
                    myMap.put(word, myMap.get(word) + 1);
                }
                isword = false;
                start = i + 1;
                // last word of String; if it doesn't end with a non letter, it
                // wouldn't count without this.
            } else if (Character.isLetter(content.charAt(i)) && i == endOfLine) {
                word = content.substring(start, i + 1);
                myMap.putIfAbsent(word, 0);
                myMap.put(word, myMap.get(word) + 1);
            }
        }

        List<Map.Entry<String, Integer>> infoIds = new ArrayList<Map.Entry<String, Integer>>(myMap.entrySet());

        //排序 by value and Key
        Collections.sort(infoIds, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return (o2.getValue() - o1.getValue()) != 0 ? o2.getValue() - o1.getValue() : o1.getKey().toString().compareTo(o2.getKey());
                //return (o1.getKey()).toString().compareTo(o2.getKey());
            }
        });
        //排序后
        for (int i = 0; i < 10; i++) {
            String id = infoIds.get(i).toString();
            System.out.println(id);
        }
        long endTime = System.currentTimeMillis();

        double duration = (endTime - startTime) / 1000.0;
        System.out.printf("Hashmap duration is %f\n", duration);

//            while (fileInput.hasNext()) {
//                content = fileInput.next();
//                int start = 0;
//                String word = "";
////                for (int i = 0; i < content.length(); i++) {
////                    char c = content.charAt(i);
////                    if (!((c < 91 && c > 64) || (c > 96 && c < 123) || (c > 59 && c < 72))) {
////                        word = content.substring(start, i);
////                        //System.out.println(word);
////                        start = i+1;
////                    }
////                }
//                if (word.equals("")) {
//                    word = content;
//                }
//
//                myMap.putIfAbsent(word, 0);
//                myMap.put(word, myMap.get(word) + 1);
//            }
    }

    static void TreeMapSort(String content) {
        long startTime = System.currentTimeMillis();
        //Map<String, Integer> Tmap = new TreeMap<String, Integer>();
        //Map<String, Integer> Tmap = new TreeMap<String, Integer>(Collections.reverseOrder());
        Map<String, Integer> Tmap = new TreeMap<String, Integer>(new Comparator<String>() {
            public int compare(String obj1, String obj2) {
                //降序排序 
                return obj2.compareTo(obj1);
            }
        });
        
        
   
        int start = 0;
        String word = "";
        boolean isword = false;
        int endOfLine = content.length() - 1;

        for (int i = 0; i < content.length(); i++) {
            // if the char is a letter, word = true.
            if (Character.isLetter(content.charAt(i)) && i != endOfLine) {
                isword = true;
                // if char isn't a letter and there have been letters before,
                // counter goes up.
            } else if (!Character.isLetter(content.charAt(i))) {
                if (isword) {
                    word = content.substring(start, i);
                    //word = word.replace(" ", "");
                    Tmap.putIfAbsent(word, 0);
                    Tmap.put(word, Tmap.get(word) + 1);
                }
                isword = false;
                start = i + 1;
                // last word of String; if it doesn't end with a non letter, it
                // wouldn't count without this.
            } else if (Character.isLetter(content.charAt(i)) && i == endOfLine) {
                word = content.substring(start, i + 1);
                Tmap.putIfAbsent(word, 0);
                Tmap.put(word, Tmap.get(word) + 1);
            }
        }
        
        Map<Integer, String> sortedMap = new TreeMap<Integer, String>(new Comparator<Integer>() {
            public int compare(Integer obj1, Integer obj2) {
                if(obj1 > obj2){ return -1; }
                else { return 1; }
            }
        });
        
        for(Entry<String,Integer> e : Tmap.entrySet()){
            sortedMap.put(e.getValue(), e.getKey());
        }
        for (Entry<Integer, String> e : sortedMap.entrySet()) {
            System.out.println(e.getKey() + ":" + e.getValue());
        }
        
        System.out.println("Tmap size: " + Tmap.entrySet().size());
        System.out.println("SortedMap size: " + sortedMap.entrySet().size());
        
//        
//        int n = 0;
//        Set<String> keySet = Tmap.keySet();
//        Iterator<String> iter = keySet.iterator();
//        while (iter.hasNext()) {
//            if (n == 10) {
//                break;
//            }
//            String key = iter.next();
//            System.out.println(key + ":" + Tmap.get(key));
//            n++;
//        }
        /*
        List<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>(Tmap.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            //升序排序
            public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
                return o2.getValue() - o1.getValue();
            }
        });
       
        int n = 0;
        for (Entry<String, Integer> e : list) {
            if (n == 10) {
                break;
            }
            System.out.println(e.getKey() + ":" + e.getValue());
            n++;
        }
         */
 /*
        
        for (Iterator i = Tmap.keySet().iterator(); i.hasNext();) {
           if(n==10)
               break;
          String key = (String) i.next();
          Integer value = (Integer) Tmap.get(key);
          System.out.println(key + " = " + value);
          n++;
        }
         */
//        for (Map.Entry<String, Integer> entry : entriesSortedByValues(Tmap)) {     
//            System.out.println(entry.getKey() + " : " + entry.getValue());
//        }
        long endTime = System.currentTimeMillis();

        double duration = (endTime - startTime) / 1000.0;
        System.out.printf("Treemap duration is %f\n", duration);
    }

    static <K, V extends Comparable<? super V>>
            SortedSet<Map.Entry<K, V>> entriesSortedByValues(Map<K, V> map) {
        SortedSet<Map.Entry<K, V>> sortedEntries = new TreeSet<Map.Entry<K, V>>(
                new Comparator<Map.Entry<K, V>>() {
            @Override
            public int compare(Map.Entry<K, V> e1, Map.Entry<K, V> e2) {
                int res = e1.getValue().compareTo(e2.getValue());
                return res != 0 ? res : 1;
            }
        }
        );
        sortedEntries.addAll(map.entrySet());
        return sortedEntries;
    }
}
