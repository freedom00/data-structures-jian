/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03linkedlistarray;

import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author trakadmin
 */
public class LinkedListArrayOfStringsTest {
    
    public LinkedListArrayOfStringsTest() {
    }
    
    @Before
    public void setUpClass() {
    }
    
    @After
    public void tearDownClass() {
    }
    
    /**
     * Test of add method, of class LinkedListArrayOfStrings.
     */
    @Test
    public void testAdd() {
        String value = "Jerry";
        LinkedListArrayOfStrings instance = new LinkedListArrayOfStrings();
        instance.add(value);
        String[] content = instance.toArray();
        assertArrayEquals("Add Jerry: ",new String[]{"Jerry"}, content); 
    }

   
}
