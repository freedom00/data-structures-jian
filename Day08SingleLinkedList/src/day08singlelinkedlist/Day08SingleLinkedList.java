/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day08singlelinkedlist;

import java.util.Iterator;

/**
 *
 * @author trakadmin
 */
public class Day08SingleLinkedList {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        LinkedListArrayOfStrings list = new LinkedListArrayOfStrings();
        list.setObserver(new LinkedListArrayOfStrings.NodeModificationObserver(){
            @Override
            public void nodeModified(LinkedListArrayOfStrings.Container node, LinkedListArrayOfStrings.NodeModificationObserver.Action action) {
                System.out.println("Node "+ node.value +" has been "+ action);
            }
        });
        for(int i=0;i<50;i++){
            list.add("AAA"+i);
        }

        for(String v : list){
            System.out.print(v + ", ");
        }
        System.out.println("");
    }
    
}


class LinkedListArrayOfStrings implements Iterable<String> {

    class LinkedListArrayOfStringsIterator implements Iterator<String> {

        private String[] valuesInOrder;
        private int currIndex;
        
        public LinkedListArrayOfStringsIterator(String[] valuesInOrder) {
            this.valuesInOrder = valuesInOrder;
        }

        @Override
        public boolean hasNext() {
            return currIndex < valuesInOrder.length;
        }

        @Override
        public String next() {
            return valuesInOrder[currIndex++];
        }        
    }

    @Override
    public Iterator<String> iterator() {
        return new LinkedListArrayOfStringsIterator(toArray());
    }
    
    public interface NodeModificationObserver{
        void nodeModified(Container node, Action action);
        enum Action{Added, Removed};
    }
    
    private NodeModificationObserver observer;
    
    void setObserver(NodeModificationObserver observer){
        this.observer = observer;
    }
    
    void callObserver(Container node, NodeModificationObserver.Action action){
        if(observer!=null)
           observer.nodeModified(node, action);
    }
    
    class Container {

        Container next;
        String value;

        public Container(String value) {
            this.value = value;
        }
    }
    Container start, end;
    int size;

    public void add(String value) {
        Container newcont = new Container(value);
        
        if(start == null){
            start = newcont;
            end = newcont;
        }else{
            end.next = newcont;
            end = newcont;
        }
        callObserver(newcont, NodeModificationObserver.Action.Added);
        size ++;
    }

    public String get(int index) {
        if(index<0 || index>size-1){
            throw new IndexOutOfBoundsException();
        }
        Container cont = start;
        int count = 0;
        while(cont!=null){
            if(count == index)
                return cont.value;
            cont = cont.next;
            count++;
        }
        return "";
    }

    public void insertValueAtIndex(int index, String value) {
        throw new UnsupportedOperationException();
    }

    public void deleteByIndex(int index) {
        throw new UnsupportedOperationException();
    }

    public boolean deleteByValue(String value) {
        throw new UnsupportedOperationException();
    } // delete first value found

    public int getSize() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String toString() {
        return String.format("[%s]", String.join(",", toArray()));
    } // comma-separated values list

    public String[] toArray() {
        String[] result = new String[size];
        int count = 0;
        Container current = start;
        while (current != null) {
            result[count++] = current.value;
            current = current.next;
        }
        return result;
    } // could be used for Unit testing
}
