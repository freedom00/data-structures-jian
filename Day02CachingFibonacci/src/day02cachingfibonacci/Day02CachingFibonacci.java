/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day02cachingfibonacci;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author trakadmin
 */
public class Day02CachingFibonacci {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        FibCached fib = new FibCached();
        System.out.println("number 8 is "+ fib.getNthFib(8));
        System.out.println("number 9 is "+ fib.getNthFib(9));
        System.out.println("number 100 is "+ fib.getNthFib(100));
    }
    
}

class FibCached {

	FibCached() {
		fibsCached.add(Long.valueOf(0)); // #0
		fibsCached.add(Long.valueOf(1)); // #1
	}

	private ArrayList<Long> fibsCached = new ArrayList<>();
	private int fibsCompCount = 2;
	// in a correct caching implementation fibsCompCount will end up the same as fibsCached.size();

	public long getNthFib(int n) {
            return computeNthFib(n);
        }
	
	// You can find implementation online, recursive or non-recursive.
	// For 100% solution you should use values in fibsCached as a starting point
	// instead of always starting from the first two values of 0, 1.
	private long computeNthFib(int n) {
            HashMap<Integer,Long> fib = new HashMap<Integer,Long>();
            
            if(n<fibsCached.size())
                return fibsCached.get(n);
            for(int i=fibsCached.size();i<=n;i++){
                fibsCached.add(fibsCached.get(i-1)+fibsCached.get(i-2));
            }
            return fibsCached.get(n);
        }
	
	// You are allowed to add another private method for fibonacci generation
	// if you want to use recursive approach. I recommend non-recursive though.

	// How many fibonacci numbers has your code computed as opposed to returned cached?
	// Use this in your testing to make sure your caching actually works properly.
	public int getCountOfFibsComputed() {
            return fibsCached.size();
        }

	@Override
	public String toString() {
            String str ="";
            for(int i=0;i<fibsCached.size();i++){
                str += i==0?fibsCached.get(i):","+fibsCached.get(i);
            }
            return str;
        } // returns all cached Fib values, comma-space-separated
	
}