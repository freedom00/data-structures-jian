/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz1rollingfifo;

import java.util.Arrays;

/**
 *
 * @author ipd
 */
public class Quiz1RollingFIFO {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try{
            RollingPriorityFIFO roll = new RollingPriorityFIFO(5);
            roll.enqueue("A", true);
            roll.enqueue("B", true);
            roll.enqueue("C", false);
            roll.enqueue("D", true);
            roll.enqueue("E", true);
            roll.dequeue();
            roll.dequeue();
            roll.dequeue();
            roll.dequeue();
            roll.dequeue();
            roll.enqueue("Z", false);
            roll.enqueue("Y", false);
            roll.enqueue("X", true);
            roll.enqueue("W", false);
            roll.enqueue("V", true);
            roll.dequeue();
//            roll.dequeue();
//            roll.dequeue();
//            roll.dequeue();
//            roll.dequeue();
//            roll.enqueue("Q", false);
//            roll.enqueue("W", true);
//            roll.enqueue("E", false);
//            roll.enqueue("R", true);
//            roll.enqueue("T", false);
//            roll.dequeue();
//            roll.dequeue();
//            roll.dequeue();
//            roll.dequeue();
//            roll.dequeue();
            String[] str = roll.toArray();
            System.out.println(""+str.length);
            System.out.println(Arrays.toString(str));
        }catch(FIFOFullException ex){
            System.out.println(ex.getMessage());
        }
    }    
}
