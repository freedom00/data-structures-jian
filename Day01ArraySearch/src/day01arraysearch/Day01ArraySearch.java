/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day01arraysearch;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author ipd
 */
public class Day01ArraySearch {

    /**
     * @param args the command line arguments
     */
 
    public static void main(String[] args) {
        // TODO code application logic here
        int[][] data2D = {
            {1, 3, 6, 8, 10},
            {3, 5, 2, 6, 5},
            {8, 3, 2, 7, 8},
            {1, 7, 3, 9, 5},
        };
        
//        int k=3;
//        int[] nums = {1,2,3,4,5,6,7};
//        int[] n1 = Arrays.copyOf(nums, k+1);
//        int[] n2 = Arrays.copyOfRange(nums, k+1, nums.length);
//        
//        System.arraycopy(n2, 0, nums, 0, n2.length);
//        System.arraycopy(n1, 0, nums, n2.length, n1.length);


        //System.out.println(""+Arrays.toString(nums));
        //System.out.println(""+data2D.length);
        //System.out.println(""+data2D[data2D.length-1].length);
        //System.out.println(""+Integer.MAX_VALUE);
        //System.out.println(""+Integer.MIN_VALUE);
        //System.out.println(Arrays.toString(getNewArr(data2D)));
        //Scanner input = new Scanner(System.in);
        //System.out.print("What is number you want to input:");
        //int total=input.nextInt();
        getTotalPairs();
        //System.out.println(Arrays.toString(getAllPairs(data2D,total)));
    }

    static int getIfExistsGentle(int[][] data, int row, int col) {
        if (row >= data.length || row < 0) {
            return 0;
        }
        if (col >= data[row].length || col < 0) {
            return 0;
        }
        return data[row][col];
    }
    
    static int getIfExistsBrutal(int[][] data, int row, int col){
        try{
            return data[row][col];
        }catch(ArrayIndexOutOfBoundsException ex){
            return 0;
        }
    }
    
    static int sumOfCross(int[][] data,int row, int col){
        int sum = getIfExistsGentle(data, row, col)+
                getIfExistsGentle(data, row-1, col)+
                getIfExistsGentle(data, row+1, col)+
                getIfExistsGentle(data, row, col-1)+
                getIfExistsGentle(data, row, col+1);
        return sum;
    }

    static int[][] getNewArr(int[][] data){
        int[][] total=new int[data.length][];
        for(int i=0;i<data.length;i++){
            total[i] = new int[data[i].length];
            for(int j=0;j<data[i].length;j++){
                //total[i][j]=sumOfCross(data,i,j);
                total[i][j]=sumOfCross(data,i,j);
            }
        }
        return total;
    }
    
    static void getAllPairs(int[][] data,int input){
        int[][] all = null;
        int w =0;
        for(int i=0;i<data.length;i++){
            for(int j=0;j<data[i].length;j++)
                for(int m=i;m<data.length;m++)
                    for(int n=0;n<data[m].length;n++){
                        w++;
                        if(m==i && n<j)
                            continue;
                        if(i==m && j==n)
                            continue;
                        if((data[i][j]+data[m][n]==input)){
                            //System.out.printf("data[%d][%d]:%d + data[%d][%d]:%d = %d\n",i,j,data[i][j],m,n,data[m][n],input);
                            
                        }
                    }                        
        }
        System.out.println(""+w);
        //return all;
    }
    
    static void getTotalPairs(){
        long w =0l;
        for(int i=0;i<100000;i++){
            for(int j=0;j<2;j++)
                for(int m=i;m<100000;m++)
                    for(int n=0;n<2;n++){
                        w++;
                    }                        
        }
        System.out.println(""+w);
        //return all;
    }
}
