/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finaltreepatterns;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ipd
 */
public class FinalTreePatterns {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws DuplicateValueException {

        TreeStringIntSet tree = new TreeStringIntSet();
        tree.setObserver(new TreeStringIntSet.NodeoObserverInt() {
            @Override
            public void visitNode(Pair<String, Integer> node, TreeStringIntSet.NodeoObserverInt.Action action) {
                try {
                    FileWriter fileWriter = new FileWriter("events.txt", true);
                    PrintWriter fileOutput = new PrintWriter(fileWriter);
                    String str = String.format("Event: %s, nodeKey=%s, value=%d",action,node.key,node.value);
                    fileOutput.println(str);
                    fileOutput.close();
                } catch (FileNotFoundException ex) {
                    System.out.println(ex.getMessage());
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        });
        
        //TreeStringIntSet.NodeStringInt node = new TreeStringIntSet.NodeStringInt("Jerry");
        //node.accept(new ComputerPartDisplayVisitor());
        
        tree.visitEachNode(new TreeStringIntSet.NodeVisitorInt(){
            @Override
            public void visitNode(TreeStringIntSet.Node node) {
                
            }            
        });
        //NodeVisitorInt computer = new TreeStringIntSet.NodeVisitorInt();
        //computer.accept(new ComputerPartDisplayVisitor());
        
        
        
        tree.add("M", 0);
        tree.add("M", 2);
        tree.add("D", 0);
        tree.add("B", 3);
        tree.add("Y", 0);
        tree.add("G", 3);
        tree.add("F", 3);
        tree.add("F", 0);
        tree.printDebug();
        System.out.println(""+tree.getKeysContainingValue(0));
        //Pair<String,Integer> pair = new Pair<>("M",1);
//        for (Pair v : tree) {
//            System.out.println(v);
//        }
    }
}

class Pair<K, V> {

    K key;
    V value;

    Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String toString() {
        return String.format("(%s=>%s)", key.toString(), value.toString());
    }
}

class DuplicateValueException extends Exception {

    public DuplicateValueException() {
    }
}

class TreeStringIntSet implements Iterable<Pair<String, Integer>> {

    class TreeStringIntSetIterator implements Iterator<Pair<String, Integer>> {

        private List<Pair<String, Integer>> keysInOrder;
        private int currIndex;

        public TreeStringIntSetIterator(List<Pair<String, Integer>> keysInOrder) {
            this.keysInOrder = keysInOrder;
        }

        @Override
        public boolean hasNext() {
            return currIndex < keysInOrder.size();
        }

        @Override
        public Pair<String, Integer> next() {
            return keysInOrder.get(currIndex++);
        }
    }

    @Override
    public Iterator<Pair<String, Integer>> iterator() {
        return new TreeStringIntSetIterator(getAllKeysAndValues());
    }

    class Node {

        Node left, right;
        String key; // keys are unique
        // HashSet is like ArrayList except it does not hold duplicates
        HashSet<Integer> valuesSet = new HashSet<>(); // unique only

        public Node(String key) {
            this.key = key;
        }
    }

    private Node root;

    void add(String key, int value) throws DuplicateValueException { // throws DuplicateValueException
        if (key.equals("")) {
            throw new DuplicateValueException();
        }

        Pair<String, Integer> pair = new Pair(key, value);

        Node node = getNodeByKey(key);
        //if (containsKey(key)) {
        if (node != null) {
            for (Integer in : node.valuesSet) {
                if (in == value) {
                    callObserver(pair, NodeoObserverInt.Action.AddFailDuplicate);
                    throw new DuplicateValueException();
                }
            }
            node.valuesSet.add(value);
            callObserver(pair, NodeoObserverInt.Action.Add);
        } else {
            node = root;
            if (node == null) {
                root = new Node(key);
                root.valuesSet.add(value);
                callObserver(pair, NodeoObserverInt.Action.Add);
                return;
            }
            Node parent;
            while (node != null) {
                int compare = key.compareTo(node.key);
                parent = node;
                if (compare < 0) {
                    node = node.left;
                    if (node == null) {
                        Node newNode = new Node(key);
                        parent.left = newNode;
                        parent.left.valuesSet.add(value);
                        callObserver(pair, NodeoObserverInt.Action.Add);
                    }
                } else {
                    node = node.right;
                    if (node == null) {
                        Node newNode = new Node(key);
                        parent.right = newNode;
                        parent.right.valuesSet.add(value);
                        callObserver(pair, NodeoObserverInt.Action.Add);
                    }
                }
            }
        }
    }

    Node getNodeByKey(String key) {
        Node node = root;
        while (node != null) {
            int compare = key.compareTo(node.key);
            if (compare == 0) {
                return node;
            } else if (compare < 0) {
                node = node.left;
            } else {
                node = node.right;
            }
        }

        return null;
    }

    boolean containsKey(String key) {
        Node node = root;
        while (node != null) {
            int compare = key.compareTo(node.key);
            if (compare == 0) {
                return true;
            } else if (compare < 0) {
                node = node.left;
            } else {
                node = node.right;
            }
        }

        return false;
    }
//

    List<Integer> getValuesByKey(String key) {
        Node node = root;
        List<Integer> list = new ArrayList<Integer>();
        while (node != null) {
            int compare = key.compareTo(node.key);
            if (compare == 0) {
                for (Integer subset : node.valuesSet) {
                    list.add(subset);
                }
                return list;
            } else if (compare < 0) {
                node = node.left;
            } else {
                node = node.right;
            }
        }
        //return empty list
        return list;
    }
//

    public void preTrvTreeValue(Node node, List<String> list, int value) {
        if (node != null) {
            for (Integer i : node.valuesSet) {
                if (i == value) {
                    list.add(node.key);
                    break;
                }
            }
            preTrvTreeValue(node.left, list, value);
            preTrvTreeValue(node.right, list, value);
        }
    }

    List<String> getKeysContainingValue(int value) {
        List<String> list = new ArrayList<String>();
        Node node = root;
        preTrvTreeValue(node, list, value);
        //return empty list
        return list;
    }

    public void preTrvTreeALLKey(Node node, List<String> list) {
        if (node != null) {
            list.add(node.key);
            preTrvTreeALLKey(node.left, list);
            preTrvTreeALLKey(node.right, list);
        }
    }

    List<String> getAllKeys() {
        List<String> list = new ArrayList<String>();
        Node node = root;
        preTrvTreeALLKey(node, list);
        //return empty list
        return list;
    }

    List<Pair<String, Integer>> getAllKeysAndValues() {
        List<Pair<String, Integer>> pairs = new ArrayList<Pair<String, Integer>>();
        List<String> keys = getAllKeys();
        for (String key : keys) {
            List<Integer> values = getValuesByKey(key);
            for (Integer value : values) {
                Pair<String, Integer> pair = new Pair(key, value);
                pairs.add(pair);
            }
        }
        return pairs;
    }

    // add code for observer that is called on each node's add/failure to modify.
    // two parameters: node, operation enum value (like we did in class)
    interface NodeoObserverInt {

        void visitNode(Pair<String, Integer> node, Action action);

        enum Action {
            Add, Modify, AddFailDuplicate
        };
    }

    private NodeoObserverInt observer;

    void setObserver(NodeoObserverInt observer) {
        this.observer = observer;
    }

    void callObserver(Pair<String, Integer> node, NodeoObserverInt.Action action) {
        if (observer != null) {
            observer.visitNode(node, action);
        }
    }
    // add code for visitor that is called on each node - parameter is node

    // Note: only one observer and one visitor at a time is supported.
    void visitEachNode(NodeVisitorInt visitor) {
        // call visitor on each node in the tree
        visitor.visitNode(this.root);
        //throw new UnsupportedOperationException();
    }

    interface NodeVisitorInt {
        void visitNode(Node node);
    }
    //-----------------------------------------------------------------------------
    //步骤 1 定义一个表示元素的接口
    public interface NodeV {
       public void accept(NodeVisitor nodeVisitor);
    }
    
    //步骤 2 创建扩展了上述类的实体类
    public class NodeStringInt  implements NodeV {
        NodeStringInt left, right;
        String key; // keys are unique
        // HashSet is like ArrayList except it does not hold duplicates
        HashSet<Integer> valuesSet = new HashSet<>(); // unique only

        public NodeStringInt(String key) {
            this.key = key;
        }
        
        @Override
        public void accept(NodeVisitor nodeVisitor) {
           nodeVisitor.visit(this);
        }
    }
    
    //步骤 3 定义一个表示访问者的接口
    public interface NodeVisitor {
       public void visit(NodeStringInt node);
    }
    
    //步骤 4 创建实现了上述类的实体访问者
    public class NodeDisplayVisitor implements NodeVisitor {
       @Override
       public void visit(NodeStringInt node) {
          System.out.println("Displaying node "+node.key+"-->"+node.valuesSet);
       }
    }
///////////////////////////////////////////////////////////////////////////////////
    public int getTreeDepth(Node root) {
        return root == null ? 0 : (1 + Math.max(getTreeDepth(root.left), getTreeDepth(root.right)));
    }

    private void writeArray(Node currNode, int rowIndex, int columnIndex, String[][] res, int treeDepth) {
        // 保证输入的树不为空
        if (currNode == null) {
            return;
        }
        // 先将当前节点保存到二维数组中
        res[rowIndex][columnIndex] = String.valueOf(currNode.key);

        // 计算当前位于树的第几层
        int currLevel = ((rowIndex + 1) / 2);
        // 若到了最后一层，则返回
        if (currLevel == treeDepth) {
            return;
        }
        // 计算当前行到下一行，每个元素之间的间隔（下一行的列索引与当前元素的列索引之间的间隔）
        int gap = treeDepth - currLevel - 1;

        // 对左儿子进行判断，若有左儿子，则记录相应的"/"与左儿子的值
        if (currNode.left != null) {
            res[rowIndex + 1][columnIndex - gap] = "/";
            writeArray(currNode.left, rowIndex + 2, columnIndex - gap * 2, res, treeDepth);
        }

        // 对右儿子进行判断，若有右儿子，则记录相应的"\"与右儿子的值
        if (currNode.right != null) {
            res[rowIndex + 1][columnIndex + gap] = "\\";
            writeArray(currNode.right, rowIndex + 2, columnIndex + gap * 2, res, treeDepth);
        }
    }

    public void printDebug() {
        if (root == null) {
            System.out.println("EMPTY!");
        }
        // 得到树的深度
        int treeDepth = getTreeDepth(root);

        // 最后一行的宽度为2的（n - 1）次方乘3，再加1
        // 作为整个二维数组的宽度
        int arrayHeight = treeDepth * 2 - 1;
        int arrayWidth = (2 << (treeDepth - 2)) * 3 + 1;
        // 用一个字符串数组来存储每个位置应显示的元素
        String[][] res = new String[arrayHeight][arrayWidth];
        // 对数组进行初始化，默认为一个空格
        for (int i = 0; i < arrayHeight; i++) {
            for (int j = 0; j < arrayWidth; j++) {
                res[i][j] = " ";
            }
        }

        // 从根节点开始，递归处理整个树
        // res[0][(arrayWidth + 1)/ 2] = (char)(root.val + '0');
        writeArray(root, 0, arrayWidth / 2, res, treeDepth);

        // 此时，已经将所有需要显示的元素储存到了二维数组中，将其拼接并打印即可
        for (String[] line : res) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < line.length; i++) {
                sb.append(line[i]);
                if (line[i].length() > 1 && i <= line.length - 1) {
                    i += line[i].length() > 4 ? 2 : line[i].length() - 1;
                }
            }
            System.out.println(sb.toString());
        }

    } // any output that helps you with debugging, recursively visit all nodes of the trees
}
