/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03linkedlistarray;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author trakadmin
 */
public class Day03LinkedListArray {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //ArrayList<String> test = new ArrayList<>();
        
        LinkedListArrayOfStrings link = new LinkedListArrayOfStrings();
        for(int i=0;i<50;i++)
            link.insertValueAtIndex(link.size,"AAA"+i);
        //link.add("A");
        //link.add("B");
        //link.add("C");
        //link.add("D");
        System.out.println(link);
        while (link.size>0) {
            link.deleteByIndex(0);
            System.out.println(Arrays.toString(link.toArray()));
        }
        link.add("E");
        //System.out.println(Arrays.toString(link.toArray()));
        //link.insertValueAtIndex(3, "F");
        //System.out.println(Arrays.toString(link.toArray()));
//        for (int i = link.size-1; i>=0; i--) {
//            link.deleteByIndex(i);
//            System.out.println(Arrays.toString(link.toArray()));
//        }
//        if(link.deleteByValue("D"))
//            System.out.println("deleted value D");
        System.out.println(Arrays.toString(link.toArray()));        
    }

}

class LinkedListArrayOfStrings {

    private class Container {
        Container next;
        String value;
    }
    Container start, end;
    int size;

    public LinkedListArrayOfStrings() {
        start = new Container();
        end = new Container();
        size = 0;
    }

    public void add(String value) {
        Container cont = new Container();
        cont.value = value;
        if (size == 0) {
            start.next = cont;
            end.next = cont;
        } else {
            Container last = end.next;
            end.next = cont;
            last.next = cont;
        }
        size++;
    }

    public String get(int index) {
        Container node = getNode(index);
        return node.value;
    }

    public void insertValueAtIndex(int index, String value) {
        if (index > size || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Out of array index");
        }
        Container cont = new Container();
        cont.value = value;
        if(index==0){
            Container first = start.next;
            start.next = cont;
            cont.next = first;
            if(first==null)
                end.next = cont;
            size++;
            return;
        }else if (index == size) {
            add(value);
            return;
        }       
        else{        
            try {
                Container nodePrevious = getNode(index-1);
                Container nodeNext = nodePrevious.next;
                nodePrevious.next = cont;
                cont.next = nodeNext;
                size++;
            } catch (NullPointerException ex) {
                throw new NullPointerException("Cannot find node");
            }
        }        
    }

    public void deleteByIndex(int index) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Out of array index");
        }

        try {
            if(index == 0){//first node
                start.next = start.next.next;
            }else {//last node
                Container nodePrevious = getNode(index-1);
                Container nodeNext = nodePrevious.next.next;
                nodePrevious.next = nodeNext; 
            }
        } catch (NullPointerException ex) {
            throw new NullPointerException("Cannot find node");
        }
        size--;
    }
    public boolean deleteByValue(String value) {
        boolean del = false;
        int i = 0;
        Container node = start.next;

        while(i < size){
            if(node.value.equals(value)){
                deleteByIndex(i);
                return true;
            }
            try {
                node = node.next;
            } catch (NullPointerException ex) {
                throw new NullPointerException("Cannot find node");
            }
            i++;
        }
        return del;
    } // delete first value found
    
    public int getSize() {
        return size;
    }

    public Container getNode(int n) {
        if (n >= size || n < 0) {
            throw new ArrayIndexOutOfBoundsException("Out of array index");
        }
        int i = 0;
        Container node = start.next;

        while (i < n) {
            i++;
            try {
                node = node.next;
            } catch (NullPointerException ex) {
                throw new NullPointerException("Cannot find node");
            }
        }
        return node;
    }

    @Override
    public String toString() { 
        return String.format("[%s]", String.join(",", toArray()));
    } // comma-separated values list
    
    public String[] toArray() { 
        String[] strArr = new String[size];
        Container node = start.next;
        for (int i = 0; i < size; i++) {
            strArr[i]=node.value;
            node = node.next;
        }
        return strArr;
    } // could be used for Unit testing
}

class LinkedListArray<T> {
    private class Container {
        Container next;
        T value;
    }
    Container start, end;
    int size;
    
    public LinkedListArray() {
        start = new Container();
        end = new Container();
        size = 0;
    }

    public void add(T value) {
        Container cont = new Container();
        cont.value = value;
        if (size == 0) {
            start.next = cont;
            end.next = cont;
        } else {
            Container last = end.next;
            end.next = cont;
            last.next = cont;
        }
        size++;
    }

    public T get(int index) {
        Container node = getNode(index);
        return node.value;
    }

    public void insertValueAtIndex(int index, T value) {
        if (index > size || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Out of array index");
        }
        Container cont = new Container();
        cont.value = value;
        if (index == size) {
            Container last = end.next;
            end.next = cont;
            last.next = cont;
        }else if(index==0){
            Container first = start.next;
            start.next = cont;
            cont.next = first;
        }else{        
            try {
                Container nodePrevious = getNode(index-1);
                Container nodeNext = nodePrevious.next;
                nodePrevious.next = cont;
                cont.next = nodeNext;
            } catch (NullPointerException ex) {
                throw new NullPointerException("Cannot find node");
            }
        }
        size++;
    }

    public void deleteByIndex(int index) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Out of array index");
        }

        try {
            if(index == 0){//first node
                start.next = start.next.next;
            }else {//last node
                Container nodePrevious = getNode(index-1);
                Container nodeNext = nodePrevious.next.next;
                nodePrevious.next = nodeNext; 
            }
        } catch (NullPointerException ex) {
            throw new NullPointerException("Cannot find node");
        }
        size--;
    }
    public boolean deleteByValue(String value) {
        boolean del = false;
        int i = 0;
        Container node = start.next;

        while(i < size){
            if(node.value.equals(value)){
                deleteByIndex(i);
                return true;
            }
            try {
                node = node.next;
            } catch (NullPointerException ex) {
                throw new NullPointerException("Cannot find node");
            }
            i++;
        }
        return del;
    } // delete first value found
    
    public int getSize() {
        return size;
    }

    public Container getNode(int n) {
        if (n >= size || n < 0) {
            throw new ArrayIndexOutOfBoundsException("Out of array index");
        }
        int i = 0;
        Container node = start.next;

        while (i < n) {
            i++;
            try {
                node = node.next;
            } catch (NullPointerException ex) {
                throw new NullPointerException("Cannot find node");
            }
        }
        return node;
    }

    @Override
    public String toString() { 
        String str="[";
        Container node = start.next;
        for (int i = 0; i < size; i++) {
            str += ((i==0)?"":", " + node.value);
            node = node.next;
        }
        str+="]";
        return str;
    } // comma-separated values list
    
    public T[] toArray() { 
        //if (elementData.getClass() != Object[].class)
        //    elementData = Arrays.copyOf(elementData, size, Object[].class);
        T[] newArr = (T[])new Object[size];
        Container node = start.next;
        for (int i = 0; i < size; i++) {
            newArr[i]=node.value;
            node = node.next;
        }
        return newArr;
    } // could be used for Unit testing
}
