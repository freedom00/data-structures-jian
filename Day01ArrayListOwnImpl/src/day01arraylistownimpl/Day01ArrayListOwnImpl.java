/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day01arraylistownimpl;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author trakadmin
 */
public class Day01ArrayListOwnImpl<T> {
    private T[] array;
    @SuppressWarnings("unchecked")
    public Day01ArrayListOwnImpl(Class<T> type, int sz) {
        array = (T[])Array.newInstance(type, sz);
    }
    public void put(int index, T item) {
        array[index] = item;
    }
    public T get(int index) { return array[index]; }
    // Expose the underlying representation:
    public T[] rep() { return array; }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Day01ArrayListOwnImpl<Integer> gai = new Day01ArrayListOwnImpl<Integer>(Integer.class, 10);
        // This now works:
        //Integer[] ia = gai.rep();

/*
        CustomArray<String> myArrlist = new CustomArray<String>(String.class);
        myArrlist.add("A");
        myArrlist.add("B");
        myArrlist.add("C");
        myArrlist.add("D");
        myArrlist.add("Z");

        myArrlist.deleteByIndex(4);
        System.out.println(myArrlist.toString());
        System.out.println(""+myArrlist.size());
        myArrlist.deleteByIndex(3);
        System.out.println(myArrlist.toString());  
        System.out.println(""+myArrlist.size());
        
        myArrlist.insertValueAtIndex("U", 0);
        System.out.println(myArrlist.toString());  
        System.out.println(""+myArrlist.size());
        
        myArrlist.insertValueAtIndex("O", myArrlist.size());
        System.out.println(myArrlist.toString());
        System.out.println(""+myArrlist.size());
        
        //System.out.println("get index value:"+myArrlist.get(5));
        
        String[] arrNew = myArrlist.getSlice(2, 1);
        System.out.println("New Array:"+Arrays.toString(arrNew));
*/
    }    
}

class CustomArrayOfInts {

	private int [] data = new int[1]; // only grows by doubling size, never shrinks
	private int size = 0;

        private void increaseArray(){
            int[] newArr = new int[data.length*2];
            for (int i = 0; i < data.length; i++) {
                newArr[i]=data[i];
            }
            data = newArr;
            
        }
        
	public int size() {
            return size;
        }
	public void add(int value) {
            if(size>=data.length)
                increaseArray();
            
            data[size]=value;
            size++;
        }
	public void deleteByIndex(int index) {
            if(index>=size||index<0)
                throw new ArrayIndexOutOfBoundsException("out of array index");
            for (int i=index; i<size-1; i++)
                data[i]=data[i+1];
            size--;
        }
	public void deleteByValue(int value) {
            for (int i = 0; i < size; i++) {
                if(data[i]==value){
                    deleteByIndex(i);
                    return;
                }
            }
        } // delete first value matching
	public void insertValueAtIndex(int value, int index) {
            if(index>size||index<0)
                throw new ArrayIndexOutOfBoundsException("out of array index");
            if(size>=data.length)
                increaseArray();
            for (int i = size; i>index ; i--)
                data[i]=data[i-1];
            data[index] = value;
            size++;
        }
	public void clear() { 
            size=0;
        }
	public int get(int index) { 
            if(index>=size||index<0)
                throw new ArrayIndexOutOfBoundsException("out of array index");
            return data[index];
        }
        
	public int[] getSlice(int startIdx, int length) {
            if(startIdx>size || startIdx<0 || startIdx+length>size || length<=0)
                throw new ArrayIndexOutOfBoundsException();
            int[] narr = new int[length];
            for (int i = 0; i < length; i++) 
                narr[i]=data[startIdx+i];
            return narr;
        }
        	
	@Override
	public String toString() {
            String str = "[";
            for (int i = 0; i < size; i++) {
                str += data[i]+", ";
            }
            if(str.length()>1)
                str = str.substring(0,str.length()-2);
            str += "]";
            return str;
        } // returns String similar to: [3, 5, 6, -23]
}

class CustomArrayOfStrings {

	private String [] data = new String[1]; // only grows by doubling size, never shrinks
	private int size = 0;

        private void increaseArray(){
            String[] newArr = new String[data.length*2];
            for (int i = 0; i < data.length; i++) {
                newArr[i]=data[i];
            }
            data = newArr;            
        }
        
	public int size() {
            return size;
        }
	public void add(String value) {
            if(size>=data.length)
                increaseArray();
            
            data[size]=value;
            size++;
        }
	public void deleteByIndex(int index) {
            if(index>=size||index<0)
                throw new ArrayIndexOutOfBoundsException("out of array index");
            for (int i=index; i<size-1; i++)
                data[i]=data[i+1];
            size--;
        }
	public void deleteByValue(String value) {
            for (int i = 0; i < size; i++) {
                if(data[i].equals(value)){
                    deleteByIndex(i);
                    return;
                }
            }
        } // delete first value matching
	public void insertValueAtIndex(String value, int index) {
            if(index>size||index<0)
                throw new ArrayIndexOutOfBoundsException("out of array index");
            if(size>=data.length)
                increaseArray();
            for (int i = size; i>index ; i--)
                data[i]=data[i-1];
            data[index] = value;
            size++;
        }
	public void clear() { 
            size=0;
        }
	public String get(int index) { 
            if(index>=size||index<0)
                throw new ArrayIndexOutOfBoundsException("out of array index");
            return data[index];
        }
        
	public String[] getSlice(int startIdx, int length) {
            if(startIdx>size || startIdx<0 || startIdx+length>size || length<=0)
                throw new ArrayIndexOutOfBoundsException("out of array index");
            String[] newarr = new String[length];
            for (int i = 0; i < length; i++) 
                newarr[i]=data[startIdx+i];
            return newarr;
        }
        	
	@Override
	public String toString() {
            String str = "[";
            for (int i = 0; i < size; i++) {
                str += data[i]+", ";
            }
            if(str.length()>1)
                str = str.substring(0,str.length()-2);
            str += "]";
            return str;
        } // returns String similar to: [3, 5, 6, -23]
}

class CustomArray<T> {
        private T[] data;
	private int size = 0;
        
        public CustomArray(Class<T> type) {
            data = (T[])Array.newInstance(type, 1);
        }
        
        @SuppressWarnings("unchecked")
        private void increaseArray(){
            T[] newArr = (T[])new Object[data.length*2];
            for (int i = 0; i < data.length; i++) {
                newArr[i]=data[i];
            }
            data = newArr;
        }
        
	public int size() {
            return size;
        }
	public void add(T value) {
            if(size>=data.length)
                increaseArray();            
            data[size]=value;
            size++;
        }
        
	public void deleteByIndex(int index) {
            if(index>=size||index<0)
                throw new ArrayIndexOutOfBoundsException("out of array index");
            for (int i=index; i<size-1; i++)
                data[i]=data[i+1];
            size--;
        }
        
	public void deleteByValue(T value) {
            for (int i = 0; i < size; i++) {
                if(data[i].equals(value)){
                    deleteByIndex(i);
                    return;
                }
            }
        } // delete first value matching
	public void insertValueAtIndex(T value, int index) {
            if(index>size||index<0)
                throw new ArrayIndexOutOfBoundsException("out of array index");
            if(size>=data.length)
                increaseArray();
            for (int i = size; i>index ; i--)
                data[i]=data[i-1];
            data[index] = value;
            size++;
        }
	public void clear() { 
            size=0;
        }
	public T get(int index) { 
            if(index>=size||index<0)
                throw new ArrayIndexOutOfBoundsException("out of array index");
            return data[index];
        }
        
	public T[] getSlice(int startIdx, int length) {
            if(startIdx>size || startIdx<0 || startIdx+length>size || length<=0)
                throw new ArrayIndexOutOfBoundsException();
            T[] newarr = (T[])new Object[length];
            for (int i = 0; i < length; i++) 
                newarr[i]=data[startIdx+i];
            return newarr;
        }
        	
	@Override
	public String toString() {
            String str = "[";
            for (int i = 0; i < size; i++) {
                str += data[i]+", ";
            }
            if(str.length()>1)
                str = str.substring(0,str.length()-2);
            str += "]";
            return str;
        } // returns String similar to: [3, 5, 6, -23]
}
