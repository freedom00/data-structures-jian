/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finaltreepatterns;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ipd
 */
public class TreeStringIntSetTest {
    
    public TreeStringIntSetTest() {
    }
    
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of iterator method, of class TreeStringIntSet.
     */
//    @Test
//    public void testIterator() {
//        System.out.println("iterator");
//        TreeStringIntSet instance = new TreeStringIntSet();
//        Iterator<Pair<String, Integer>> expResult = null;
//        Iterator<Pair<String, Integer>> result = instance.iterator();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of add method, of class TreeStringIntSet.
//     */
    @Test
    public void testAdd() throws DuplicateValueException {
        System.out.println("add");
        List<Integer> list = new ArrayList<Integer>();
        
        TreeStringIntSet instance = new TreeStringIntSet();
        instance.setObserver(new TreeStringIntSet.NodeoObserverInt() {
            @Override
            public void visitNode(Pair<String, Integer> node, TreeStringIntSet.NodeoObserverInt.Action action) {
                try {
                    FileWriter fileWriter = new FileWriter("events.txt", true);
                    PrintWriter fileOutput = new PrintWriter(fileWriter);
                    String str = String.format("Event: %s, nodeKey=%s, value=%d",action,node.key,node.value);
                    fileOutput.println(str);
                    fileOutput.close();
                } catch (FileNotFoundException ex) {

                } catch (IOException ex) {

                }
            }
        });
        instance.add("M", 0);
        list.add(0);
        // TODO review the generated test code and remove the default call to fail.
        assertArrayEquals(list.toArray(), instance.getValuesByKey("M").toArray());
        instance.add("M", 1);
        list.add(1);
        // TODO review the generated test code and remove the default call to fail.
        assertArrayEquals(list.toArray(), instance.getValuesByKey("M").toArray());
    }
    
    @Test(timeout = 1000)
    public void testDuplicateValueException() throws DuplicateValueException {
        System.out.println("DuplicateValueException");
        TreeStringIntSet instance = new TreeStringIntSet();
        try {
            instance.add("M", 0);
            instance.add("M", 1);
            instance.add("M", 0);
            assertTrue("Exception expected on add value 0 to M", false);
        } catch (DuplicateValueException ex) {

        }    
    }
//
//    /**
//     * Test of containsKey method, of class TreeStringIntSet.
//     */
    @Test
    public void testContainsKey() throws DuplicateValueException {
        System.out.println("containsKey");
        String key = "";
        TreeStringIntSet instance = new TreeStringIntSet();
        instance.add("M", 0);
        boolean expResult = true;
        boolean result = instance.containsKey("M");
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
//
//    /**
//     * Test of getValuesByKey method, of class TreeStringIntSet.
//     */
    @Test
    public void testGetValuesByKey() throws DuplicateValueException {
        System.out.println("getValuesByKey");
        String key = "M";
        TreeStringIntSet instance = new TreeStringIntSet();
        instance.add(key, 0);
        instance.add(key, 1);
        instance.add(key, 2);
        List<Integer> expResult = new ArrayList<Integer>();
        expResult.add(0);
        expResult.add(1);
        expResult.add(2);
        List<Integer> result = instance.getValuesByKey(key);
        assertArrayEquals(expResult.toArray(), result.toArray());
        // TODO review the generated test code and remove the default call to fail.
    }
//
//    /**
//     * Test of preTrvTreeValue method, of class TreeStringIntSet.
//     */
//    @Test
//    public void testPreTrvTreeValue() {
//        System.out.println("preTrvTreeValue");
//        TreeStringIntSet.Node node = null;
//        List<String> list = null;
//        int value = 0;
//        TreeStringIntSet instance = new TreeStringIntSet();
//        instance.preTrvTreeValue(node, list, value);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getKeysContainingValue method, of class TreeStringIntSet.
//     */
    @Test
    public void testGetKeysContainingValue() throws DuplicateValueException {
        System.out.println("getKeysContainingValue");
        int value = 3;
        TreeStringIntSet instance = new TreeStringIntSet();
        instance.add("M", 0);
        instance.add("M", 1);
        instance.add("N",0);
        instance.add("M", 2);
        instance.add("F", 0);
        instance.add("M", 3);
        List<String> expResult = new ArrayList<String>();
        expResult.add("M");
        List<String> result = instance.getKeysContainingValue(value);
        assertArrayEquals(expResult.toArray(), result.toArray());
        expResult.add("F");
        expResult.add("N");
        result = instance.getKeysContainingValue(0);
        assertArrayEquals(expResult.toArray(), result.toArray());
    }
//
//    /**
//     * Test of preTrvTreeALLKey method, of class TreeStringIntSet.
//     */
//    @Test
//    public void testPreTrvTreeALLKey() {
//        System.out.println("preTrvTreeALLKey");
//        TreeStringIntSet.Node node = null;
//        List<String> list = null;
//        TreeStringIntSet instance = new TreeStringIntSet();
//        instance.preTrvTreeALLKey(node, list);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getAllKeys method, of class TreeStringIntSet.
//     */
    @Test
    public void testGetAllKeys() throws DuplicateValueException {
        System.out.println("getAllKeys");
        TreeStringIntSet instance = new TreeStringIntSet();
        instance.add("M", 0);
        instance.add("M", 1);
        instance.add("N",0);
        instance.add("M", 2);
        instance.add("F", 0);
        instance.add("M", 3);
        List<String> expResult = new ArrayList<String>();
        expResult.add("M");
        expResult.add("F");
        expResult.add("N");
        List<String> result = instance.getAllKeys();
        assertArrayEquals(expResult.toArray(), result.toArray());

    }
//
//    /**
//     * Test of getAllKeysAndValues method, of class TreeStringIntSet.
//     */
//    @Test
//    public void testGetAllKeysAndValues() {
//        System.out.println("getAllKeysAndValues");
//        TreeStringIntSet instance = new TreeStringIntSet();
//        List<Pair<String, Integer>> expResult = null;
//        List<Pair<String, Integer>> result = instance.getAllKeysAndValues();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of setObserver method, of class TreeStringIntSet.
//     */
//    @Test
//    public void testSetObserver() {
//        System.out.println("setObserver");
//        TreeStringIntSet.NodeoObserverInt observer = null;
//        TreeStringIntSet instance = new TreeStringIntSet();
//        instance.setObserver(observer);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of callObserver method, of class TreeStringIntSet.
//     */
//    @Test
//    public void testCallObserver() {
//        System.out.println("callObserver");
//        Pair<String, Integer> node = null;
//        TreeStringIntSet.NodeoObserverInt.Action action = null;
//        TreeStringIntSet instance = new TreeStringIntSet();
//        instance.callObserver(node, action);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of visitEachNode method, of class TreeStringIntSet.
//     */
//    @Test
//    public void testVisitEachNode() {
//        System.out.println("visitEachNode");
//        TreeStringIntSet.NodeVisitorInt visitor = null;
//        TreeStringIntSet instance = new TreeStringIntSet();
//        instance.visitEachNode(visitor);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getTreeDepth method, of class TreeStringIntSet.
//     */
//    @Test
//    public void testGetTreeDepth() {
//        System.out.println("getTreeDepth");
//        TreeStringIntSet.Node root = null;
//        TreeStringIntSet instance = new TreeStringIntSet();
//        int expResult = 0;
//        int result = instance.getTreeDepth(root);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of printDebug method, of class TreeStringIntSet.
//     */
//    @Test
//    public void testPrintDebug() {
//        System.out.println("printDebug");
//        TreeStringIntSet instance = new TreeStringIntSet();
//        instance.printDebug();
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    
}
