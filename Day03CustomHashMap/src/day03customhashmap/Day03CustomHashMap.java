/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03customhashmap;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import static java.util.Objects.hash;

/**
 *
 * @author trakadmin
 */
public class Day03CustomHashMap {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CustomHashMapStringString custHash = new CustomHashMapStringString();
        for (int i = 0; i < 100; i++) {
            custHash.putValue("Jerry" + i, "Student" + i);
        }
        //custHash.putValue("Jerry", "Student");
        //custHash.putValue("Marry", "Reception");
        //custHash.putValue("Berry", "Cashier");

        //System.out.println("Total items :" + custHash.totalItems);
        custHash.printDebug();
//        System.out.println("Has a key 'Terry':" + custHash.hasKey("Terry"));
//        System.out.println("Has a key 'Jerry':" + custHash.hasKey("Jerry"));
//        System.out.println("Berry's value:" + custHash.getValue("Berry"));
        //System.out.println(custHash);

        //System.out.println("Jerry33's value:" + custHash.getValue("Jerry33"));
        custHash.delKey("Jerry0");
        custHash.printDebug();
    }
}

class CustomHashMapStringString {

    private class Container {

        Container next;
        String key;
        String value;
    }

    CustomHashMapStringString() {
        for (int i = 0; i < 7; i++) {
            Container index = new Container();
            hashTable[i] = index;
        }
        totalItems = 0;
    }

    Container[] hashTable = new Container[7];

    int totalItems;

    private int computeHash(String input) {
        int hash = 0;

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            hash += c;
            hash <<= 1;
        }
        return Math.abs(hash);
    }

    String getValue(String key) {
        int index = computeHash(key) % hashTable.length;
        Container node = hashTable[index].next;

        while (node != null) {
            if (node.key.equals(key)) {
                return node.value;
            }

            node = node.next;
        }
        return "";
    }

    void putValue(String key, String value) {//add at beginning
        expandSize();
        int index = computeHash(key) % hashTable.length;
        Container newNode = new Container();
        newNode.key = key;
        newNode.value = value;
        Container node = hashTable[index];

        boolean isExisting = false;
        while (node.next != null) {
            if (node.key != null && node.key.equals(key)) {
                node.value = value;
                isExisting = true;
                break;
            }
            node = node.next;
        }
        if (!isExisting) {
            node.next = newNode;
        }
        totalItems++;
    }

    boolean delKey(String key) {
        boolean isExisting = false;
        //outerloop:
        //for (int i = 0; i < hashTable.length; i++) {
            int index = computeHash(key)%hashTable.length;
            Container node = hashTable[index];

            while (node.next != null) {
                if (node.next.key != null && node.next.key.equals(key)) {
                    node.next = node.next.next;
                    isExisting = true;
                    break;
                    //break outerloop;
                }
                node = node.next;
            }
        //}
        if (!isExisting) {
            return false;
        }
        totalItems--;

        return isExisting;
    }

    void expandSize() {
        if (totalItems > 2 * hashTable.length) {
            int nextPrimeNumber = nextPrimeNumber(2 * hashTable.length);
            Container[] newTable = new Container[nextPrimeNumber];
            //newTable = Arrays.copyOf(hashTable, nextPrimeNumber);
            for (int i = 0; i < newTable.length; i++) {
                Container index = new Container();
                newTable[i] = index;
            }
            for (int i = 0; i < hashTable.length; i++) {
                Container node = hashTable[i].next;
                if (node != null) {
                    newTable[i].next = node;
                }
            }
            hashTable = newTable;
        }
    }

    private boolean isPrime(int value) {
        for (int i = 2; i < value; i++) {
            if (value % i == 0) {
                return false;
            }
        }
        return true;
    }

    private int nextPrimeNumber(int minValue) {
        int current = minValue;
        while (true) {
            if (isPrime(current)) {
                return current;
            }
            current++;
        }
    }
    
// LATER: expand hashTable by about *2 when totalItems > 3*hashTable.length
//	
    boolean hasKey(String key) {
        for (int i = 0; i < hashTable.length; i++) {
            Container node = hashTable[i].next;
            while (node != null) {
                if (node.key.equals(key)) {
                    return true;
                }

                node = node.next;
            }
        }
        return false;
    }
//

    public void printDebug() {
        for (int i = 0; i < hashTable.length; i++) {
            System.out.printf("Entry %d:\n", i);
            int count = 0;
            Container node = hashTable[i].next;
            while (node != null) {
                System.out.println("\t- " + node.key + " : " + node.value);
                count++;
                node = node.next;
            }
            //System.out.printf("Entry %d: %s\t", i, count == 0 ? "no items" : count + " items");
        }
        System.out.println("");
    } // print hashTable content, one entry per line, with all items in it.
//

    @Override
    public String toString() {
        String str = "[";
        for (int i = 0; i < hashTable.length; i++) {
            Container node = hashTable[i].next;
            while (node != null) {
                str += node.key + " => " + node.value + ", ";
                node = node.next;
            }
        }
        if (str.length() > 1) {
            str = str.substring(0, str.length() - 2);
        }
        str += "]";
        return str;
    } // comma-separated values->key pair list
//	// e.g. [ Key1 => Val1, Key2 => Val2, ... ]
}

class CustomHashMap<K, V> {

    private class Container {

        Container next;
        K key;
        V value;
    }

    CustomHashMap() {
        for (int i = 0; i < 5; i++) {
            Container index = new Container();
            hashTable[i] = index;
        }
        totalItems = 0;
    }

    Container[] hashTable = (Container[]) new Object[5];

    int totalItems;

    private int computeHash(V input) {
        int count = 0;

        for (int i = 0; i < hashTable.length; i++) {
            Container node = hashTable[i].next;
            while (node != null) {
                if (node.value.equals(input)) {
                    count++;
                }

                node = node.next;
            }
        }
        return count;
    }

    V getValue(K key) {
        int index = Math.abs(hash(key) << 1) % hashTable.length;
        Container node = hashTable[index].next;

        while (node != null) {
            if (node.key.equals(key)) {
                return node.value;
            }

            node = node.next;
        }
        return null;
    }

    void putValue(K key, V value) {
        expandSize();
        int index = Math.abs(hash(key) << 1) % hashTable.length;
        Container newNode = new Container();
        newNode.key = key;
        newNode.value = value;
        Container node = hashTable[index];

        boolean isExisting = false;
        while (node.next != null) {
            if (node.key != null && node.key.equals(key)) {
                node.value = value;
                isExisting = true;
                break;
            }
            node = node.next;
        }
        if (!isExisting) {
            node.next = newNode;
        }
        totalItems++;
    }

    void expandSize() {
        if (totalItems > 3 * hashTable.length) {
            Container[] newTable = (Container[]) new Object[hashTable.length * 2];
            for (int i = 0; i < newTable.length; i++) {
                Container index = new Container();
                newTable[i] = index;
            }
            for (int i = 0; i < hashTable.length; i++) {
                Container node = hashTable[i].next;
                if (node != null) {
                    newTable[i].next = node;
                }
            }
            hashTable = newTable;
        }
    }

// LATER: expand hashTable by about *2 when totalItems > 3*hashTable.length
//	
    boolean hasKey(K key) {
        for (int i = 0; i < hashTable.length; i++) {
            Container node = hashTable[i].next;
            while (node != null) {
                if (node.key.equals(key)) {
                    return true;
                }

                node = node.next;
            }
        }
        return false;
    }
//

    public void printDebug() {
        for (int i = 0; i < hashTable.length; i++) {
            int count = 0;
            Container node = hashTable[i].next;
            while (node != null) {
                count++;
                node = node.next;
            }
            System.out.printf("Entry %d: %s\t", i, count == 0 ? "no items" : count + " items");
        }
        System.out.println("");
    } // print hashTable content, one entry per line, with all items in it.
//

    @Override
    public String toString() {
        String str = "[";
        for (int i = 0; i < hashTable.length; i++) {
            Container node = hashTable[i].next;
            while (node != null) {
                str += node.key + " => " + node.value + ", ";
                node = node.next;
            }
        }
        if (str.length() > 1) {
            str = str.substring(0, str.length() - 2);
        }
        str += "]";
        return str;
    } // comma-separated values->key pair list
//	// e.g. [ Key1 => Val1, Key2 => Val2, ... ]
}
