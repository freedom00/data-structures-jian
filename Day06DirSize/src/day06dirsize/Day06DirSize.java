/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day06dirsize;

import java.io.File;

/**
 *
 * @author ipd
 */
public class Day06DirSize {

    static long getDirSizeInBytes(File directory) {
        long length = 0;
        File[] files = directory.listFiles();
        if(files == null)
            return 0;
        for (File file : files) {
            if (file.isFile()) {
                length += file.length();
            } else if(file.isDirectory()){
                length += getDirSizeInBytes(file);
            }
        }
        return length;
    }

    public static void main(String[] args) {
        // TODO code application logic here
        long size = getDirSizeInBytes(new File("C:\\Users\\6155181\\Documents"));
        System.out.printf("%,20d\n",size);
    }

}
