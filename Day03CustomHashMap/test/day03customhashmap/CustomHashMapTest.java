/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03customhashmap;

import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author trakadmin
 */
public class CustomHashMapTest {
    
    public CustomHashMapTest() {
    }
    
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getValue method, of class CustomHashMap.
     */
    @Test
    public void testGetValue() {
        System.out.println("Object getValue");
        String key = "Jerry";
        String value = "Student";
        CustomHashMapStringString instance = new CustomHashMapStringString();
        instance.putValue(key, value);
        String expResult = value;
        String result = instance.getValue(key);
        assertEquals(expResult, result);
    }

}
