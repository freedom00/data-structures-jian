package day06treeagain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Day06TreeAgain {

    public static void main(String[] args) throws DuplicateValueException {
        TreeAgain tree = new TreeAgain();
        for(int i=0;i<100;i++){
            int random = (int)(Math.random() * 100 + 1);
            tree.put(random);
        }
        tree.printDebug();
        System.out.println("Sum : " + tree.getSumOfAllValues(tree.root));
//        ArrayList<String> listOne = new ArrayList<>(Arrays.asList("a", "b", "c", "d", "f"));
//        ArrayList<String> listTwo = new ArrayList<>(Arrays.asList("a", "b", "c", "d", "e"));
//        listOne.addAll(listTwo);    //Merge both lists
//        System.out.println(listOne);
        System.out.println("" + Arrays.toString(tree.getValuesInOrder()));
        
        int[] arr = tree.getValuesInOrder();
        for(int i=0;i<arr.length;i++){
            int random = (int)(Math.random() * 100 + 1);
            System.out.println("remove "+arr[i]+":");
            tree.remove(arr[i]);
            tree.printDebug();
            System.out.println("");
        }
    }
}

class DuplicateValueException extends Exception {

    public DuplicateValueException(String msg) {
        super();
    }
}

//tree of unique values
class TreeAgain {

    private class NodeOfInt {

        NodeOfInt left, right, parent;
        int value;

        public NodeOfInt(int value) {
            this.value = value;
        }
    }

    NodeOfInt root;
    int nodesCount;

    void put(int value) throws DuplicateValueException {
        if (hasValue(value)) {
            return;
            //throw new DuplicateValueException("Has this value: " + value);
        }
        NodeOfInt node = root;
        if (node == null) {
            root = new NodeOfInt(value);
            nodesCount++;
            return;
        }
        NodeOfInt parent;
        while (node != null) {
            int compare = value - node.value;
            parent = node;
            if (compare < 0) {
                node = node.left;
                if (node == null) {
                    parent.left = new NodeOfInt(value);
                    parent.left.parent = parent;
                }
            } else {
                node = node.right;
                if (node == null) {
                    parent.right = new NodeOfInt(value);
                    parent.right.parent = parent;
                }
            }
        }
        nodesCount++;
    }

    int getSumOfAllValues(NodeOfInt node) {
        int sum = 0;
        sum += node.value;
        if (node.left != null) {
            sum += getSumOfAllValues(node.left);
        }
        if (node.right != null) {
            sum += getSumOfAllValues(node.right);
        }

        return sum;
    }

    boolean hasValue(int value) {
        NodeOfInt node = root;
        while (node != null) {
            int compare = value - node.value;
            if (compare == 0) {
                return true;
            } else if (compare < 0) {
                node = node.left;
            } else {
                node = node.right;
            }
        }
        return false;
    }

    NodeOfInt hasValueNode(int value) {
        NodeOfInt node = root;
        while (node != null) {
            int compare = value - node.value;
            if (compare == 0) {
                return node;
            } else if (compare < 0) {
                node = node.left;
            } else {
                node = node.right;
            }
        }
        return null;
    }

    NodeOfInt getPrevNode(NodeOfInt node) {
        int value = node.value;
        int[] arr = getValuesInOrder();
        for (int i = 1; i < arr.length - 1; i++) {
            if (value == arr[i]) {
                value = arr[i - 1];
                break;
            }
        }
        return hasValueNode(value);
    }

    NodeOfInt getNextNode(NodeOfInt node) {
        int value = node.value;
        int[] arr = getValuesInOrder();
        for (int i = 1; i < arr.length - 1; i++) {
            if (value == arr[i]) {
                value = arr[i + 1];
                break;
            }
        }
        return hasValueNode(value);
    }

    NodeOfInt getMinValueNode(NodeOfInt node) {
        if (node.left != null) {
            return getMinValueNode(node.left);
        } else {
            return node;
        }
    }

    boolean remove(int value) {
        NodeOfInt node = hasValueNode(value);
        if (node == null) {
            return false;
        }
        //Deleting a leaf
        if (node.left == null && node.right == null) {
            if (node.parent.left == node) {
                node.parent.left = null;
            }
            if (node.parent.right == node) {
                node.parent.right = null;
            }
            node.parent = null;
            return true;
        }

        //Deleting a node with one child
        if (node.left == null || node.right == null ) {
            if (node.left != null) {
                if (node.parent.left == node) {
                    node.parent.left = node.left;
                } 
                if (node.parent.right == node)  {
                    node.parent.right = node.left;                    
                }
                node.left.parent = node.parent;
            }
            if (node.right != null) {
                if(node != root){
                    if (node.parent.left == node) {
                        node.parent.left = node.right;
                    } else {
                        node.parent.right = node.right;
                    }
                    node.right.parent = node.parent;
                }else{
                    root = node.right;
                }
            }

            node.parent = null;
            return true;
        }
//Deleting a node with two children???????if dont has right child.......if has right child,replace to min value of right child, if min value(never has left child) has right child, replace ........
        if (node.right != null) {//use min value of node of right child to replace it.
            NodeOfInt minNode = getMinValueNode(node.right); //76
            if (minNode.right != null) {//if min node has right child(naver has left child), use right child replace min node place.
                NodeOfInt minRightChild = minNode.right;
                minRightChild.parent = minNode.parent;
                if (minNode.parent.left == minNode) {
                    minNode.parent.left = minRightChild;
                }
                if (minNode.parent.right == minNode) {
                    minNode.parent.right = minRightChild;
                }
            }else{//leaf
                if(minNode.parent.left == minNode)
                    minNode.parent.left = null;
                if(minNode.parent.right == minNode)
                    minNode.parent.right = null;
            }
            //------------------move min node to replaced node place.
            minNode.parent = node.parent;
            minNode.left = node.left;
            if(node.right != null && node.right!=minNode){
                minNode.right = node.right;
                node.right.parent = minNode;
            }
                
            if (node.left != null) {
                node.left.parent = minNode;
            }
            if (node.parent!=null && node.parent.left == node) {
                node.parent.left = minNode;
            }
            if (node.parent!=null && node.parent.right == node) {
                node.parent.right = minNode;
            }
            if(node==root)
                root = minNode;
        }

        node = null;

        nodesCount--;
        return true;
    }

    int getValuesCount() {

        return nodesCount;
    }

    ArrayList<Integer> getArrayList(NodeOfInt node) {

        ArrayList<Integer> arrayList = new ArrayList<>();

        if (node.left != null) {
            arrayList.addAll(getArrayList(node.left));
        }

        arrayList.add(node.value);

        if (node.right != null) {
            arrayList.addAll(getArrayList(node.right));
        }
        return arrayList;
    }

    int[] getValuesInOrder() {
        ArrayList<Integer> total = getArrayList(root);
        Integer[] array = total.toArray(new Integer[total.size()]);
        return Arrays.stream(array).mapToInt(Integer::intValue).toArray();
    } // from smallest to largest, no sorting

    public int getTreeDepth(NodeOfInt root) {
        return root == null ? 0 : (1 + Math.max(getTreeDepth(root.left), getTreeDepth(root.right)));
    }

    private void writeArray(NodeOfInt currNode, int rowIndex, int columnIndex, String[][] res, int treeDepth) {
        // 保证输入的树不为空
        if (currNode == null) {
            return;
        }
        // 先将当前节点保存到二维数组中
        res[rowIndex][columnIndex] = String.valueOf(currNode.value);

        // 计算当前位于树的第几层
        int currLevel = ((rowIndex + 1) / 2);
        // 若到了最后一层，则返回
        if (currLevel == treeDepth) {
            return;
        }
        // 计算当前行到下一行，每个元素之间的间隔（下一行的列索引与当前元素的列索引之间的间隔）
        int gap = treeDepth - currLevel - 1;

        // 对左儿子进行判断，若有左儿子，则记录相应的"/"与左儿子的值
        if (currNode.left != null) {
            res[rowIndex + 1][columnIndex - gap] = "/";
            writeArray(currNode.left, rowIndex + 2, columnIndex - gap * 2, res, treeDepth);
        }

        // 对右儿子进行判断，若有右儿子，则记录相应的"\"与右儿子的值
        if (currNode.right != null) {
            res[rowIndex + 1][columnIndex + gap] = "\\";
            writeArray(currNode.right, rowIndex + 2, columnIndex + gap * 2, res, treeDepth);
        }
    }

    public void printDebug() {
        if (root == null) {
            System.out.println("EMPTY!");
        }
        // 得到树的深度
        int treeDepth = getTreeDepth(root);

        // 最后一行的宽度为2的（n - 1）次方乘3，再加1
        // 作为整个二维数组的宽度
        int arrayHeight = treeDepth * 2 - 1;
        int arrayWidth = (2 << (treeDepth - 2)) * 3 + 1;
        // 用一个字符串数组来存储每个位置应显示的元素
        String[][] res = new String[arrayHeight][arrayWidth];
        // 对数组进行初始化，默认为一个空格
        for (int i = 0; i < arrayHeight; i++) {
            for (int j = 0; j < arrayWidth; j++) {
                res[i][j] = " ";
            }
        }

        // 从根节点开始，递归处理整个树
        // res[0][(arrayWidth + 1)/ 2] = (char)(root.val + '0');
        writeArray(root, 0, arrayWidth / 2, res, treeDepth);

        // 此时，已经将所有需要显示的元素储存到了二维数组中，将其拼接并打印即可
        for (String[] line : res) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < line.length; i++) {
                sb.append(line[i]);
                if (line[i].length() > 1 && i <= line.length - 1) {
                    i += line[i].length() > 4 ? 2 : line[i].length() - 1;
                }
            }
            System.out.println(sb.toString());
        }

    } // any output that helps you with debugging, recursively visit all nodes of the trees
}
